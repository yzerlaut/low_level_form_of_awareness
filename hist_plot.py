import os, sys
sys.path.append(os.path.expanduser('~')+os.path.sep+'work')
import matplotlib.pylab as plt
from graphs.my_graph import *
import scipy.special as spec

fig2, ax2 = plt.subplots(figsize=(2.5,1.8))
for data, Qe, Qi, pconn, Ntot, Te, Ti, i in zip(
        [np.load('data/active_vm.npz'), np.load('data/VA_vm.npz')],
        [1., 6.],
        [4., 67.],
        [0.05, 0.02],
        [5000, 10000],
        [5e-3, 5e-3],
        [5e-3, 10e-3],
        range(2)):
    args = data['args'].all()
    fe, fi = data['EXC_ACTS'][0][1000:].mean(), data['INH_ACTS'][0][1000:].mean()
    Ge = fe*0.8*Ntot*pconn*Te*Qe/10
    Gi = fi*0.2*Ntot*pconn*Ti*Qi/10
    print(Ge, Gi)
    ax2.bar([i], [1+np.log(1+Ge+Gi)/np.log(10)])
ax2.bar([i+1], [1])
set_plot(ax2, yticks=[1,2,3], yticks_labels=['1', '10', '100'], ylabel='$G_{tot}/G_L$', xticks=[])
plt.tight_layout()
fig2.savefig('fig.svg')
show(plt)

# fig1, ax1 = plt.subplots(figsize=(4,2))
# for data, alpha in zip([np.load('data/active_vm.npz'), np.load('data/VA_vm.npz')], [1.,0.8]):
#     vv = np.empty(0)
#     for i in range(len(data['VMS'])):
#         for j in range(len(data['VMS'][i])):
#             vv = np.concatenate([vv, data['VMS'][i][j][1000:]])
#             print(data['VMS'][i][j][1000:])
#     ax1.hist(vv, alpha=alpha, normed=True)
# set_plot(ax1, yticks=[], ylabel='density', xlabel='$V_m$ (mV)', xticks=[-75,-65,-55])
# fig1.savefig('fig.svg')



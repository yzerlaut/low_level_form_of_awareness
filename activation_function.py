import os, sys
sys.path.append(os.path.expanduser('~')+os.path.sep+'work')
import matplotlib.pylab as plt
from graphs.my_graph import *
import scipy.special as spec

###############################################
############# cortical parameters #############
###############################################

## ---- membrane parameters
Gl, Cm , El = 10e-9, 200e-12, -70e-3 
Ee, Ei = 0, -80e-3 # synaptic reversal potential
# integrate and fire properties
Vthre=-50e-3
refrac = 5e-3 ; 
## ---- excitatory synaptic conductances
Qe, Te = 1e-9, 5e-3
## ---- inhibitory synaptic conductances
Qi, Ti = 4e-9, 5e-3
Ntot = 5000

## ---- activated state parameters
fe_up, fi_up = 4, 6 # Hz, stationary activity levels
## ---- low conductance state parameters
fe_down, fi_down = 1e-12, 1e-12 # Hz, stationary activity levels

## ---- network parameters
pconnec = 0.05 # connection probability between neurons
g = 0.2 # 20 % of inhibitory neurons

###############################################
############# thalamic parameters #############
###############################################

########## thalamo-cortical afference
Te_thal = Te # same for cortico-cortical and thalamo-cortical synapses

t = np.linspace(0, 100, 1e3)*1e-3

def PSP(t, Nsyn, Qsyn, Tsyn, Tm, Cm, E, mVm):
    """ function that give the PSP time course after the linearization of Kuhn et al. 2004,
    Equation 15 in the paper
    with also the case Tm=Tsyn (not discussed in the paper, but pretty trivial)"""
    if abs(Tm-Tsyn)>1e-10:
        A = Tsyn*Qsyn*Tm/(Cm*(Tm-Tsyn))*(E-mVm)
        psp = Nsyn*A*(np.exp(-t/Tm)-np.exp(-t/Tsyn))
    else:
        psp = Nsyn*Qsyn*t*(E-mVm)/Cm*np.exp(-t/Tm)
    return psp


def membrane_pot_fluct(fe, fi, Qe, Qi, Te, Ti, pconnec, Ntot, g):
    """ Kuhn et al. 2004 calculus of mean and variance of the Vm"""
    Fe, Fi = fe*pconnec*(1-g)*Ntot, fi*pconnec*g*Ntot
    muGe, muGi = Qe*Te*Fe, Qi*Ti*Fi
    muG = Gl+muGe+muGi
    muV = (muGe*Ee+muGi*Ei+Gl*El)/muG
    muGn, Tm = muG/Gl, Cm/muG
    
    Ue, Ui = Qe/muG*(Ee-muV), Qi/muG*(Ei-muV)

    sV = np.sqrt(Fe*(Ue*Te)**2/2./(Te+Tm)+Fi*(Qi*Ui)**2/2./(Ti+Tm))

    Fe, Fi = Fe+1e-9, Fi+1e-9 # just to insure a non zero division, 
    # Tv = ( Fe*(Ue*Te)**2 + Fi*(Qi*Ui)**2 ) /( Fe*(Ue*Te)**2/(Te+Tm) + Fi*(Qi*Ui)**2/(Ti+Tm) )

    return muV, sV+1e-12, Tm

def activation_prob(STIM, mVm, sVm, Vthre=Vthre):
    """ activation function of the network
    allows to calculate the recruitment as a response to STIM,
    Equation 11 in the paper"""
    numerator = spec.erf((Vthre-mVm)/(np.sqrt(2)*sVm)) - spec.erf((Vthre-mVm-STIM)/(np.sqrt(2)*sVm))
    denom = spec.erf((Vthre-mVm)/(np.sqrt(2)*sVm)) + 1
    return numerator/denom

def gaussian_func(vv, mVm, sVm):
    """ classic, Equation 4 in the paper"""
    return np.exp(-(vv-mVm)**2/2./sVm**2)/np.sqrt(2*np.pi)/sVm

def fraction_baseline_firing(mVm, sVm, Vthre=Vthre):
    """ number of neurons not available for the stimulus evoked response
    because they already fire due to the baseline firing rate
    this is equal to Nbg/Ntot in the paper"""
    return 0.5*(1-spec.erf((Vthre-mVm)/(np.sqrt(2)*sVm))) # 0 when Vthre>>0, 1 when Vthre<<0 


# we start by calculating the mean and variance of membrane potential in the cortical network
mVm_up_cort, sVm_up_cort, Tm_up_cort = membrane_pot_fluct(fe_up, fi_up, Qe, Qi, Te, Ti, pconnec, Ntot, g)
mVm_down_cort, sVm_down_cort, Tm_down_cort = membrane_pot_fluct(fe_down, fi_down, Qe, Qi, Te, Ti, pconnec, Ntot, g)
print(mVm_up_cort, sVm_up_cort, Tm_up_cort)

#### ACTIVATION functions in the 2 states (applies to the "available" neurons)
## SAME WITH HIGHER DISCRETIZATION
Qe_thal = np.linspace(0.1, 150.,1e3)*1e-9

max_depol_up_cort, max_depol_down_cort = [0.*Qe_thal for i in range(2)]
for i in range(len(Qe_thal)):
    PSP_up_cort = PSP(t, 1, Qe_thal[i], Te_thal, Tm_up_cort, Cm, Ee, mVm_up_cort)
    PSP_down_cort = PSP(t, 1, Qe_thal[i], Te_thal, Tm_down_cort, Cm, Ee, mVm_down_cort)
    max_depol_up_cort[i] = PSP_up_cort.max()
    max_depol_down_cort[i] = PSP_down_cort.max()
Response_RSFS = activation_prob(max_depol_up_cort, mVm_up_cort, sVm_up_cort)
Response_Quiesc = activation_prob(max_depol_down_cort, mVm_down_cort, sVm_down_cort)

###############################################
############# cortical parameters #############
###############################################


## ---- membrane parameters
Gl, Cm , El = 10e-9, 150e-12, -60e-3 
Ee, Ei = 0, -80e-3 # synaptic reversal potential
# integrate and fire properties
Vthre=-50e-3; refrac = 5e-3 ; 
## ---- excitatory synaptic conductances
Qe, Te = 7e-9, 5e-3
## ---- inhibitory synaptic conductances
Qi, Ti = 67e-9, 10e-3
Ntot = 5000

## ---- network parameters
pconnec = 0.02 # connection probability between neurons
g = 0.2 # 20 % of inhibitory neurons

## ---- activated state parameters
fe_up, fi_up = 24., 24. # Hz, stationary activity levels
## ---- low conductance state parameters
fe_down, fi_down = 0., 0.0 # Hz, stationary activity levels

###############################################
############# thalamic parameters #############
###############################################

# we start by calculating the mean and variance of membrane potential in the cortical network
mVm_up_cort, sVm_up_cort, Tm_up_cort = membrane_pot_fluct(fe_up, fi_up, Qe, Qi, Te, Ti, pconnec, Ntot, g)
print(mVm_up_cort, sVm_up_cort, Tm_up_cort)
max_depol_up_cort = 0.*Qe_thal 
for i in range(len(Qe_thal)):
    PSP_up_cort = PSP(t, 1, Qe_thal[i], Te_thal, Tm_up_cort, Cm, Ee, mVm_up_cort)
    max_depol_up_cort[i] = PSP_up_cort.max()
Response_VA = activation_prob(max_depol_up_cort, mVm_up_cort, sVm_up_cort)

fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.plot(Qe_thal[Qe_thal>20e-9]/10e-9, Response_RSFS[Qe_thal>20e-9], lw=3, label="RS-FS")
ax.plot(Qe_thal[Qe_thal>20e-9]/10e-9, Response_Quiesc[Qe_thal>20e-9], ':', lw=3, label="quiescent")
ax.plot(Qe_thal[Qe_thal>20e-9]/10e-9, Response_VA[Qe_thal>20e-9], lw=3, label="VA")
ax.legend(loc='upper left', prop={'size':'small'}, frameon=False)
set_plot(ax, xlabel='synaptic weight \n (fraction of leak conductance)', ylabel='spiking proba.', ylim=[-0.02,1.02],\
         yticks=[0,0.5, 1], xticks=[5,10])
plt.tight_layout()
fig.savefig('fig1.svg')


fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.plot(Qe_thal[Qe_thal<20e-9]/10e-9, Response_RSFS[Qe_thal<20e-9], lw=3, label="RS-FS")
ax.plot(Qe_thal[Qe_thal<20e-9]/10e-9, Response_Quiesc[Qe_thal<20e-9], lw=3, label="quiescent")
ax.plot(Qe_thal[Qe_thal<20e-9]/10e-9, Response_VA[Qe_thal<20e-9], lw=3, label="RS-FS")
ax.legend(loc='upper left', prop={'size':'small'}, frameon=False)
set_plot(ax, xlabel='synaptic weight \n (fraction of leak conductance)', ylabel='spiking proba.', ylim=[-0.02,1.02],\
         yticks=[0,0.5, 1], xlim=[0,2.1], xticks=[0, 0.25, 1, 2], xticks_labels=['0','', '1', '2'])
plt.tight_layout()
fig.savefig('fig2.svg')

show(plt)

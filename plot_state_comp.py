import os, sys
sys.path.append(os.path.expanduser('~')+os.path.sep+'work')
import matplotlib.pylab as plt
from graphs.my_graph import *
from graphs.traces_plots import set_scale_and_annotation
from scipy.ndimage.filters import gaussian_filter1d

FF = 2
N = 20000

fig, AX = plt.subplots(3, 1, figsize=(5,10))
fig2, ax2 = plt.subplots(1, figsize=(3,1.8))
plt.subplots_adjust(left=0.25, bottom=0.15, wspace=0.2, hspace=0.2)
for exp, factor in zip(['SR', 'VA', 'active'], [4, 4, 1]):
    data = np.load('data/'+exp+'.npz')
    args = data['args'].all()
    # full_act = .2*data['INH_ACTS']+.8*data['EXC_ACTS']
    full_act = data['EXC_ACTS']
    variations = full_act.std(axis=0).mean()
    t_zoom = -20+np.arange(60/args.DT)*args.DT+1
    AX[0].plot(np.arange(500)*args.DT, gaussian_filter1d(full_act[-1][-500:],2), label=exp)
    AX[1].plot(np.arange(500)*args.DT, gaussian_filter1d(full_act[-1][-500:],2), label=exp)
    trace, counter = 0.*t_zoom, 0
    for spike_times, exc_act in zip(data['SPK_TIMES'], full_act):
        i_plot = int(data['SPK_TIMES'].shape[0]*len(np.unique(spike_times))/20)
        for t_spk in np.unique(spike_times):
            i_spk = int(t_spk/args.DT)
            # exc_act[i_spk+int(t_zoom[0]/args.DT)-10*FF:i_spk+int(t_zoom[-1]/args.DT)+1+10*FF] = \
            #   gaussian_filter1d(exc_act[i_spk+int(t_zoom[0]/args.DT)-10*FF:i_spk+int(t_zoom[-1]/args.DT)+1+10*FF],FF)
            to_add = exc_act[i_spk+int(t_zoom[0]/args.DT):i_spk+int(t_zoom[-1]/args.DT)+1]
            if len(to_add)!=len(t_zoom):
                print('size pb')
            else:
                counter +=1
                trace += to_add
        print(counter)
    to_plot = (trace-trace[t_zoom<0].mean())/counter/variations
    AX[2].plot(t_zoom, 100.*to_plot/factor, lw=2, label=exp)
    ax2.plot(np.cumsum(100.*to_plot/factor)*args.DT)

# then quiescent state data
AX[0].plot(np.arange(500)*args.DT, np.zeros(500), label='Quiesc.')
AX[1].plot(np.arange(500)*args.DT, np.zeros(500), label='Quiesc.')
AX[2].plot(t_zoom, 0.*t_zoom, lw=2, label='Quiesc.')

set_plot(AX[0], xlabel='time (ms)', ylabel='pop. act. (Hz)', yticks=[500, 1000], ylim=[100,2000])
set_plot(AX[1], xlabel='time (ms)', ylabel='pop. act. (Hz)', ylim=[-3,40], yticks=[0,14,30])
set_plot(AX[2], xlabel='time lag (ms)', xticks=[-10,0,10,20,30],
         ylabel='norm. evoked response \n '+r'$( \langle \nu \rangle_{stim.} - \mu_{\nu} ) / \sigma_{\nu} $,  % ', xlim=[-10,30])
AX[0].legend()
set_scale_and_annotation(ax2, yunit='ms', xunit='ms', xscale=100, yscale=10)
fig2.savefig('fig.svg')
show(plt)
# plt.show()

